# Introduction to the object-oriented-programming with java

## Content
- draw forms like triangle and square on console
- get used with setter and getter methods

## Clone the project
```bash
git clone https://gitlab.com/bbwrl/m319-geometrische-formen.git
```

## contribute to the project
Teachers of the School BBW can
- contribute to the project by adding a pull request.
- contribute by writing a message to the author.

