package ch.bbw.gforms;

public class Diamond {
    private int width=8;
    private int height=8;

    public void draw() {
        // todo: implement the diamond draw method
        System.out.println("diamond draw method");
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public int getWidth() {
        return width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public int getHeight() {
        return height;
    }
}
