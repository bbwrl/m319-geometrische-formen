package ch.bbw.gforms;

public class TriangleStanding {
    private int height=8;

    public void draw() {
        int line = 1;
        int width = height*2;
        while (line<=height) {
            int spaces=width/2-line;
            for (int i=0; i<spaces; i++) System.out.print("-");
            for (int i=0; i<(width-(spaces*2)); i++) System.out.print("*");
            for (int i=0; i<spaces; i++) System.out.print("-");
            System.out.println();
            line++;
        }
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public int getHeight() {
        return height;
    }

}
