package ch.bbw.gforms;

public class MainApplicationDiamond {

    public static void main(String[] args) {
        Diamond diamond = new Diamond();
        diamond.setWidth(8);
        diamond.setHeight(8);
        diamond.draw();
    }
}
