package ch.bbw.gforms;

public class Rectangle {
    private int width=8;
    private int height=8;

    public void draw() {
        for (int i=0; i<height; i++) {
            for (int j=0; j<=width; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public int getWidth() {
        return width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public int getHeight() {
        return height;
    }
}
