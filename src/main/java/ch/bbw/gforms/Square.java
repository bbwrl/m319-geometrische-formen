package ch.bbw.gforms;

public class Square {
    private int width=8;

    public void draw() {
        for (int i=0; i<width; i++) {
            for (int j=0; j<=width; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public int getWidth() {
        return width;
    }
}
