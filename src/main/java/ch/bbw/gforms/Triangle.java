package ch.bbw.gforms;

public class Triangle {
    private int height=8;

    public void draw() {
        for (int i=0; i<height; i++) {
            for (int j=0; j<=i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public int getHeight() {
        return height;
    }

}
