package ch.bbw.gforms;

import java.util.Scanner;

public class MainApplication {
    public static void main(String[] args) {
        // variant 1
        Triangle triangle1 = new Triangle();
        triangle1.draw();

        // variant 2
        Triangle triangle2 = new Triangle();
        triangle2.setHeight(5);
        triangle2.draw();

        // variant 3
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Geben Sie die Dreieckshöhe ein:");
        int height = keyboard.nextInt();

        Triangle triangle3 = new Triangle();
        triangle3.setHeight(height);
        triangle3.draw();

        keyboard.close();
    }
}
